<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

//Route::resource('short-url', 'ShortUrlController');

Route::post('get-short-url', 'ShortUrlController@getShortUrl');
Route::post('get-short-url-array', 'ShortUrlController@getShortUrlArray');
Route::get('get-urls', 'ShortUrlController@getUrlList');
Route::get('/{codeUrl}', 'ShortUrlController@getOriginalUrl');
