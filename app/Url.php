<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    //
    protected $fillable = ['urlShort', 'urlLong'];

    public $timestamps = false;

    public function scopeGetUrlShort($query, $urlLong){
        return $query->where('urlLong', $urlLong);
    }

    public function scopeGetUrlLong($query, $urlShort){
        return $query->where('urlShort', $urlShort);
    }
}
