<?php

namespace App\Http\Controllers;

use App\services\Validation\UrlValidator;
use App\services\Helpers\Helpers;

use Illuminate\Http\Request;

class ShortUrlController extends Controller
{
    protected $helper;
    protected $validation;
    public function __construct()
    {
        $this->helper = new Helpers();
        $this->validation = new UrlValidator();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function getShortUrl(Request $request){
        $validate = $this->validation->validateUrl($request);

        $response = array(
            'status' => 'error',
            'url' => '',
            'errors'=> null
        );

        if(is_null($validate)){
            try{
                $url = $this->helper->generateUrlShort($request->url);
                $response['status'] = 'ok';
                $response['url'] = $url;
                return response()->json($response);

            }
            catch (\Exception $e){
                $response['message'] = $e->getMessage();
                return response()->json($response);
            }
        }else{
            $response['errors'] = $validate;
            return response()->json($response);
        }
    }

    public function getShortUrlArray(Request $request){
        $response = array(
            'status' => 'error',
            'urls' => array(),
            'message' => ''
        );
        if(isset($request->filetxt)){
            $urls = $this->helper->readTxt($request->filetxt);
            try{
                $urlsArray = $this->helper->generateUrlArray($urls);
                $response['status'] = 'ok';
                $response['urls'] = $urlsArray;
            }
            catch (\Exception $e){
                $response['message'] = $e->getMessage();
            }

        }else{
            if(isset($request->url)){
                if(is_array($request->url)){
                    try{
                        $urlsArray = $this->helper->generateUrlArray($request->url);
                        $response['status'] = 'ok';
                        $response['urls'] = $urlsArray;
                    }
                    catch (\Exception $e){
                        $response['message'] = $e->getMessage();
                    }
                }
            }
        }
        return response()->json($response);
    }

    public function getOriginalUrl($codeUrl){
        $host= $_SERVER["HTTP_HOST"];
        $urlShort = 'http://'.$host.'/'.$codeUrl;
        $response = array(
            'status' => 'error',
            'redirect' => false,
            'url' => ''
        );

        try{
            $urlOriginal = $this->helper->getUrlOriginal($urlShort);
            if(!is_null($urlOriginal)){
                return redirect($urlOriginal);
            }else{
                return response()->json($response);
            }
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function getUrlList(){
        $urls = $this->helper->getUrlList();
        return response()->json($urls);
    }
}
