<?php

namespace App\Http\Controllers;

use App\services\Validation\UrlValidator;
use App\services\Helpers\Helpers;

use Illuminate\Http\Request;

class RedirectController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($codeUrl)
    {
        $host= $_SERVER["HTTP_HOST"];
        $url  = 'http://'.$host.'/api/'.$codeUrl;
        return redirect($url);
    }
}
