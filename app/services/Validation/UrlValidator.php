<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 29/10/18
 * Time: 05:26 PM
 */

namespace App\services\Validation;
use Illuminate\Support\Facades\Validator;


class UrlValidator
{

    public function validateUrl($request){
        $message = null;
        $validator = Validator::make($request->all(), [
            'url' => 'required|active_url',
        ]);
        if ($validator->fails()) {
            $message = $validator->errors();
        }

        return $message;

    }

}
