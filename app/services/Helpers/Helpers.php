<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 29/10/18
 * Time: 05:54 PM
 */

namespace App\services\Helpers;


use App\Url;

class Helpers
{
    public function getUrlOriginal($urlShort){
        $urlOriginal = null;
        $urlObj = Url::GetUrlLong($urlShort)->first();
        if(!is_null($urlObj)){
            //Asgino el de la DB
            $dataUrl = $urlObj->toArray();
            $urlOriginal = $dataUrl['urlLong'];
        }
        return $urlOriginal;
    }

    public function generateUrlShort($urlLong){
        $urlShort = "";
        $urlObj = Url::GetUrlShort($urlLong)->first();
        if(!is_null($urlObj)){
            //Asgino el de la DB
            $dataUrl = $urlObj->toArray();
            $urlShort = $dataUrl['urlShort'];
        }else{
            //Genero una nueva url corta
            $host= $_SERVER["HTTP_HOST"];
            $urlLast = $this->urlRandom();
            $urlShort = 'http://'.$host.'/'.$urlLast;
            $url = new Url();
            $url->urlShort = $urlShort;
            $url->urlLong = $urlLong;
            $url->save();
        }
        return $urlShort;
    }

    public function getUrlList(){
        $urls = Url::get()->toArray();
        return $urls;
    }

    public function readTxt($fileTxt){
        $urls = array();
        $file = fopen($fileTxt, 'r');
        while(!feof($file))
        {
            array_push($urls, fgets($file));
        }
        fclose($file);

        unset($urls[count($urls) - 1]);

        return $urls;
    }

    public function generateUrlArray($urls){
        $urlsArray = array();
        foreach ($urls as $url){
                $urlShort = $this->generateUrlShort($url);
                array_push($urlsArray, $urlShort);
        }
        return $urlsArray;
    }

    private function urlRandom(){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $length = 5;
        $stringRandom = '';
        for ($i = 0; $i < $length; $i++) {
            $stringRandom .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $stringRandom;
    }
}
