
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('short-url-component', require('./components/ShortUrlComponent.vue'));
Vue.component('list-url-component', require('./components/ListUrlComponent.vue'));
Vue.component('prueba2-component', require('./components/Prueba2Component.vue'));

const app = new Vue({
    el: '#app',
    data(){
        return {
            section:'inicio',
        };
    },
    methods:{
        selectSection(section){
            this.section = section;
        }
    }
});
