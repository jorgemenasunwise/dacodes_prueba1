export default {
    shortenUrl:(request) =>{
        return axios.post("./api/get-short-url", request).then((response) => Promise.resolve(response.data)).catch((error) => Promise.reject(error));
    },

    getUrls:()=>{
        return axios.get("./api/get-urls").then((response) => Promise.resolve(response.data)).catch((error) => Promise.reject(error));
    },

    shortenUrlTxt:(request) =>{
        return axios.post("./api/get-short-url-array", request).then((response) => Promise.resolve(response.data)).catch((error) => Promise.reject(error));
    },
}
