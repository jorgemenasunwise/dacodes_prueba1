<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Acortador URL</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">

    </head>
    <body>
        <div id="app" class="container" >
            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="javascript:void(0)">Acortador</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link" href="javascript:void(0)" @click="selectSection('inicio')">Inicio</a>
                        <a class="nav-item nav-link" href="javascript:void(0)" @click="selectSection('lista')">Lista de Url</a>
                    </div>
                </div>
            </nav>
            <template v-if="section=='inicio'">
                <short-url-component v-if></short-url-component>
            </template>
            <template v-if="section=='lista'">
                <list-url-component></list-url-component>
            </template>
        </div>
        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>
