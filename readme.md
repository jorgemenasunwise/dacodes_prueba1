# Prueba 1
## Descripción general

El proyecto esta desarrollado en PHP y en mysql, por tal motivo deben estar instalados esos dos
servicios en el equipo donde se instalará, dentro de la carpeta principal
se encuentra un respaldo de la DB solo se debe importar a una base de datos
que se llame "dacodes_prueba1".

## Requeriemintos técnicos
- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- DB Mysql

## Instalación

- Clonar el repositorio y poner la carpeta ya sea en htdocs o en www.
- Crear un virtual host en su equipo que apunte a la carpeta "public" del proyecto.
- Reiniciar el servidor.
- Crear una DB en mysql, llamarle "dacodes_prueba1" y asigarle un usuario
- Importar a la DB el archivo "dacodes_prueba1.sql"
- Configurar el .env del proyecto con los datos del usuario asignado a la DB
- Ejecutar "composer install" dentro de la carpeta del proyecto para instalar las dependencias php necesarias.
- Listo el proyecto ya debe correr
- Nota: Si da un problema de permisos cambiar los permisos de la carpeta del proyecto a "777" (chmod 777 -R dacodes_prueba1)

## API
    Acortar URL
    http://vhostname/api/get-short-url
    
    Acortar URL enviando un Array
    http://vhostname/api/get-short-url-array
    
Las colecciones se encuentran en la carpeta raiz del proyecto para poder ser probado en postman.


# Prueba 2

La prueba 2 se encuentra dentro del mismo proyecto para acceder a el es necesario tener instalado
el proyecto, la url para ingresar es http://vhostname/prueba2, y el archivo del codigo fuente se encuentra en
"resources/js/components/Prueba2Component.vue"
